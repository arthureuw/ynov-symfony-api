#### Quel est l'intérêt de créer une API plutôt qu'une application classique ?

- Une API permet d'echanger les memes donnees sur plusieurs platforme, ou entre plusieurs API. (ex: La SNCF utilise une API pour avoir une application mobile et une platforme web. Dans le cas de la SNCF ils utilisent aussi des API pour echanger des donnees entre API).

#### Résumez les étapes du mécanisme de sérialisation implémenté dans Symfony

- Le serialisation permet de transformer un Objet symfony en un autre format (JSON, XML...), dans le cas de notre API ce sera du JSON.

#### Qu'est-ce qu'un groupe de sérialisation ? A quoi sert-il ?

- Un groupe de serialisation permet de selectionner les donnees serialise a envoyer, l'utilisateur n'a pas besoin d'avoir toute les informations (ex: date de creation, mot de passe...).

#### Quelle est la différence entre la méthode PUT et la méthode PATCH ?

- La Methode PUT est tres differente de PATCH de par la maniere dont il actualise les donnees, PUT supprime toute les donnees et les re-ajoute, PATCH va seulement modifier la/les donnees a chan ger son toucher a l'integrite des autres donnees non modifier en base

#### Quels sont les différents types de relation entre entités pouvant être mis en place avec Doctrine ?

- ManyToOne, OneToMany, ManyToMany, OneToOne.

#### Qu'est-ce qu'un Trait en PHP et à quoi peut-il servir ?

- Un trait en PHP permet d'externaliser et de separer du Code, ce sont des methodes qui peuvent etre utilise dans des classes. Un trait en php evite beaucoup de duplication de code.
