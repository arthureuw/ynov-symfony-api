<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article_list", methods={"GET"})
     * @param ArticleRepository $articleRepository
     * @return JsonResponse
     */
    public function list(
        ArticleRepository $articleRepository
    )
    {
        $articles = $articleRepository->findAll();

        return $this->json([
            'article' => $articles
        ]);
    }

    /**
     * @Route("/article/{id}", name="article_detail", methods={"GET"})
     * @param Article $article
     * @return JsonResponse
     */
    public function detail(Article $article)
    {
        return $this->json([
            'article' => $article
        ]);
    }

    /**
     * @Route("/article", name="article_create", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response|JsonResponse
     */
    public function create(
        Request $request,
        EntityManagerInterface $em
    ) {
        $data = json_decode($request->getContent(), true);
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($article);
            $em->flush();

            return $this->json(
                $article,
                Response::HTTP_CREATED,
                [],
                ["groups" => "car:create"]
            );
        }
        return new Response("ok");
    }
}
