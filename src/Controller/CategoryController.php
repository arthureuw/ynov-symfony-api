<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="category_list", methods={"GET"})
     * @param CategoryRepository $categoryRepository
     * @return JsonResponse
     */
    public function list(
        CategoryRepository $categoryRepository
    )
    {
        $category = $categoryRepository->findAll();

        return $this->json([
            'category' => $category
        ]);
    }

    /**
     * @Route("/article/{id}", name="article_detail", methods={"GET"})
     * @param Category $category
     * @return JsonResponse
     */
    public function detail(Category $category)
    {
        return $this->json([
            'category' => $category
        ]);
    }

    /**
     * @Route("/article", name="article_create", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response|JsonResponse
     */
    public function create(
        Request $request,
        EntityManagerInterface $em
    ) {
        $data = json_decode($request->getContent(), true);
        $article = new Category();
        $form = $this->createForm(CategoryType::class, $article);

        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($article);
            $em->flush();

            return $this->json(
                $article,
                Response::HTTP_CREATED,
                [],
                ["groups" => "car:create"]
            );
        }
        return new Response("ok");
    }
}
