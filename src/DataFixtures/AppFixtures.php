<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $faker = (new Factory())::create('fr_FR');

        for ($i = 0; $i < 5; $i++) {
            $category = new Category();
            $category->setName($faker->sentence($nbWords = 2, $variableNbWords = false));
            $category->setCreated($faker->dateTime);
            $manager->persist($category);
        }

        for ($i = 0; $i < 10; $i++) {
            $article = new Article();
            $article->setTitle($faker->sentence($nbWords = 4, $variableNbWords = true));
            $article->setContent($faker->sentence($nbWords = 20, $variableNbWords = true));
            $article->setStatus($faker->numberBetween(0,2));
            $article->setTrending($faker->boolean);
            $article->setPublished($faker->dateTime);
            $article->setCreated($faker->dateTime);
            $article->setCategory($category);
            $manager->persist($article);
        }
        $manager->flush();
    }
}
